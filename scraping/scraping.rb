require 'rubygems'
require 'mechanize'
require 'csv'

#needs a random function
# WEBSITES = [
# 	,
# 	{
# 		name: ECONOMICOS_NAME,
# 		base_url: "http://www.economicos.cl/",
# 		search_params: [
			
# 		],
# 		links_selector: ".results.left a",
# 		heading_selector: ".cont_tit_detalle_f h1",
# 		address_selector: ".cont_tit_detalle_f h3",
# 		description_selector: "#description p",
# 		contact_name_selector: "ul.vendor > li.cont_ecn_name_vendor",
# 		# owner_name_selector: - not used as contact_name == owner_name
# 		phone_selector: "#contact-options strong#phone",
# 		price_selector: ".cont_price_detalle_f"
# 	}
# ]

base_path = File.expand_path(File.dirname(__FILE__))
csv_path = base_path + '/results.csv'
log_path =  base_path + "/scraping.log"
log_file = File.open(log_path, File::WRONLY | File::CREAT)
$log = Logger.new(log_file)
# To create new (and to remove old) logfile, add File::CREAT like:

def custom_log(msg)
	$log.info msg
	puts msg
end

ECONOMICOS_NAME = "Economicos EMOL"
PORTALINM_NAME = "Portal Inmobiliario"

class WebsiteScraper
	def initialize(init_config)
		@name = init_config[:name]
		@base_url = init_config[:base_url]
		@search_params = init_config[:search_params]
		@selectors = init_config[:selectors]
		@listing_pages = []
		@results = []

		@scraper = init_config[:scraper]
	end

	def generate_links
		@search_params.each do |search_param|
			@listing_pages = @listing_pages + (yield search_param) #yield must return an array
		end
	end

	def scrape

		#traverses array of links
		@listing_pages.each do |listing_page|
			#grabs a link and pases the body - body has list of links inside a tags
			@scraper.get(listing_page) do |listing_body|
				puts "Currently on #{listing_page}" 
				property_listing_selector = @selectors[:property_listing]
				#inside the body there are links listing properties
				begin
					listing_body.search(property_listing_selector).each do |property_link|
						#selects all such links
						property_url = property_link[:href].strip
						@scraper.get(property_url) do |property_body|
							#redirects to a new page that contains the property detail
							result = yield property_body, @selectors
							if result != false
								@results << result
							end
						end
					end
				rescue
					puts "An error ocurred. Scraping continues."
				end
			end
		end
	end

	def get_results
		return @results
	end
end

economicos_links_proc = Proc.new do |config|
  links = []
  n = config[:start]
  m = config[:finish]
  (n..m).each do |k|
  	base_url = config[:base_url]
  	link = "#{base_url}/rm/propiedades?pagina=#{k}\#results"
  	links << link
  end
  links
end

portal_links_proc = Proc.new do |config|
  links = []
  n = config[:start]
  m = config[:finish]
  skip = config[:skip]
  op = config[:operation]
  prop = config[:property]
  (n..m).each do |k|
    unless skip.include? k
      base_path = "http://www.portalinmobiliario.com"
      suffix_path = "metropolitana?ca=2&ts=1&mn=2&or=f-des&sf=0&sp=0&at=0&pg="
      # suffix_path = "santiago-metropolitana?ca=2&ph=350000&ts=1&dh=2&mn=1&or=f-des&sf=0&sp=0&at=0&pg="
      page_number = k
      link = "#{base_path}/#{op}/#{prop}/#{suffix_path}#{page_number}"
      links << link
    end
  end
  #implicitly returning links
  #cannot use return statement as it would halt execution inside of the method
  links
end

economicos_data_proc = Proc.new do |property_page, config|
	result = nil
end


#property page includes method from scraper such as search
portal_data_proc = Proc.new do |property_page, config|

	# puts "scraping listing..."

	# the result is assigned to a variable since we're using a Proc
	# code needs to be refactored into a lambda format 
	# to be able to return with the return keyword explicitly.
	result = nil

	is_potential_client = nil

	match_regex = /propiedad|corredor|corretaje|arriendo|inmobiliaria|asociado|abogado|amoblado|venta|inmobiliario|raices|fullhaus|casa|inmueble|arquitecto|apart|hotel|administracion|procarvallo|asesoria|conygomez|gerencia|gestion|info|prop|inversiones|property|homie|home|homy|raeb/i

	heading_selector = config[:heading]
	address_selector = config[:address]
	description_selector = config[:description]
	contact_name_selector = config[:contact_name]
	price_selector = config[:price]
	county_selector = config[:county]
	post_date_selector = config[:post_date]
	area_selector = config[:area]
	configuration_selector = config[:configuration]

	property_type = property_page.uri.to_s.split("/")[4]

	heading = property_page.search(heading_selector).text.strip
	county = property_page.search(county_selector).text.strip.downcase
	address = property_page.search(address_selector).text.strip
	description = property_page.search(description_selector).text.strip
	contact_name = property_page.search(contact_name_selector).text
	price = property_page.search(price_selector).text.strip
	post_date = property_page.search(post_date_selector).text.strip.split(" ")[1]
	area = property_page.search(area_selector).text.strip
	configuration = property_page.search(configuration_selector).text.strip

	contact_string = ""
	owner_name = ""
	owner_phone = ""
	owner_email = ""

	#must go to strip tags to retrieve the information
	script_tags = property_page.search("script").each do |script_tag|
		text = script_tag.text
		if text.include? "datosContacto"
			contact_string = text
			parsed_data = contact_string.split("};")[1].split(",")
			owner_name = parsed_data[0]
			owner_phone = parsed_data[1]
			owner_email = parsed_data[2]
		end
	end

	property_url = property_page.canonical_uri.to_s

	operation = property_url.split("/")[3]
	image_src = ""

	result = [
		operation,
		property_type,
		county,
		post_date,
		heading, 
		address,
		description,
		price, 
		area,
		configuration,
		contact_name,
		owner_name,
		owner_email,
		owner_phone,
		property_url,
		"PortalInm",
		contact_string,
		image_src
	]

	if contact_name.length > 0
		is_potential_client = false
	end	

	if owner_email
		if (owner_email.match? match_regex)
			is_potential_client = false
		end
	end

	if owner_phone
		if (owner_phone.match? match_regex)
			is_potential_client = false
		end
	end

	if owner_name	
		if (owner_name.match? match_regex)
			is_potential_client = false
		end
	end

	if is_potential_client == false
		is_potential_client = "N"
	else
		is_potential_client = "Y"
	end

	result << is_potential_client

	# implicit return
	result
	
end



scraper = Mechanize.new { |agent|
	agent.user_agent_alias = "Mac Safari"
}

scraper.history_added = Proc.new {
	sleep_seconds = rand(1..2) * rand(4..6) * 0.05
	# custom_log "Sleeping #{sleep_seconds}"
}

EconomicosScraper = WebsiteScraper.new ({
	name: ECONOMICOS_NAME,
	base_url: "http://www.economicos.cl",
	search_params: [
		{start: 0, finish: 12}
	]
});

PortalScraper  = WebsiteScraper.new ({
		name: PORTALINM_NAME,
		base_url: "http://www.portalinmobiliario.com",
		search_params: [
				{operation: 'arriendo', property: 'casa', start: 1, finish: 15, skip: []},
				{operation: 'venta', property: 'casa', start: 1, finish: 30, skip: []},
				{operation: 'venta', property: 'departamento', start: 1, finish: 30, skip: []},
				{operation: 'arriendo', property: 'departamento', start: 1, finish: 30, skip: []},
		],
		selectors: {
			property_listing: ".products-list a",
			heading: "h4.media-block-title",
			county: ".data-sheet-column-address p span:nth-of-type(2)",
			address: "h4.media-block-title",
			description: ".propiedad-descr",
			contact_name: "p.operation-contact-name",
			price: ".media-block-descr p.price",
			post_date: "p.operation-internal-code:nth-of-type(2) strong",
			area: ".data-sheet-column-area p",
			configuration: ".data-sheet-column-programm p" #note the double m - incorrect misspelling by portalinmobiliario
		},
		scraper: scraper
});

PortalScraper.generate_links &portal_links_proc
PortalScraper.scrape &portal_data_proc

results = (PortalScraper.get_results)

results_header = [
	"operacion",
	"tipo propiedad",
	"comuna",
	"fecha publicacion",
	"Titulo",
	"direccion",
	"Descripcion",
	"precio",
	"superficie",
	"configuracion",
	"nombre contacto",
	"nombre",
	"email",
	"fono",
	"URL",
	"portal",
	"info contacto",
	"fuente imagen",
	"potencial cliente"
]

results.insert(0, results_header)

custom_log "Scraping ends..."

custom_log "Writing to CSV file..."

CSV.open(csv_path, "w+") do |csv_file|
	results.each do |row|
		csv_file << row
	end
end