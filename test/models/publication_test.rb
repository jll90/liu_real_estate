require 'test_helper'

class PublicationTest < ActiveSupport::TestCase

	def setup
		@platform = Platform.first
		@property_one = Property.first
		@property_two = Property.last

		@publication_one = Publication.new(
			property_id: @property_one.id,
			platform_id: @platform.id,
			days_to_update: 3
		)

		@publication_two = Publication.new(
			property_id: @property_one.id,
			platform_id: @platform.id,
			days_to_update: 4	
		)
	end


	test "publication is valid" do
		assert @publication_one.valid?
	end

	test "can save publication" do
		assert_difference "Publication.count", 1 do
			@publication_one.save
		end
	end

	test "days to update is required" do
		@publication_one.days_to_update = ""
		assert_not @publication_one.valid?
	end

	test "property_id is required" do
		@publication_one.property_id = nil
		assert_not @publication_one.valid?
	end

	test "platform_id is required" do
		@publication_one.platform_id = nil
		assert_not @publication_one.valid?
	end

	test "days to update must be gt zero" do
		@publication_one.days_to_update = 0
		assert_not @publication_one.valid?
	end

	test "publication two is valid" do
		assert @publication_two.valid?
	end

	test "publication two cannot be saved, b/c duplication" do
		@publication_one.save
		assert_no_difference "Publication.count" do
			@publication_two.save
		end
	end
end