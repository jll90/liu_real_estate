require 'test_helper'

class PlatformTest < ActiveSupport::TestCase
  def setup
  	@platform = Platform.new(
  		website: "abc",
  		name: "afda",
  		days_to_update: 3
  		)
  end

  test "platform is valid" do
  	assert @platform.valid?
  end

  test "can save platform" do
  	assert_difference "Platform.count", 1 do
  		@platform.save
  	end
  end

  test "must have a website" do
  	@platform.website = ""
  	assert_not @platform.valid?
  end

  test "must have a name" do
  	@platform.name = ""
  	assert_not @platform.valid?
  end

  test "must have days to update" do
  	@platform.days_to_update = nil
  	assert_not @platform.valid?
  end

  test "days to update must be an integer gt zero" do
  	@platform.days_to_update = 0
  	assert_not @platform.valid?
  end
end