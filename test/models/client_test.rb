require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  def setup
  	@client = Client.new(fullname: "fullname", status: 0)
  end

  test "client is valid" do
  	assert @client.valid?
	end

	test "fullname must not be blank" do
		@client.fullname = ""
		assert_not @client.valid?
	end

	test "status must be a positive int" do
		@client.status = -1
		assert_not @client.valid?
	end
end