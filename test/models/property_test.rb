require 'test_helper'

class PropertyTest < ActiveSupport::TestCase
	def setup
	  @property = Property.new(
	  	  permalink: 'permalink-one',
	  	  title: 'this is the title',
				latitude: 43.4324,
				longitude: 42.43243,
				fake_full_address: 'fake_address',
				street_name: 'fdasf',
				street_number: '4324',
				state_id: State.first.id,
				county_id: County.first.id,
				city_id: City.first.id,
				pricing_unit: "CLP"
	  	)

	  @repeated_permalink_property = Property.new(
	  		permalink: 'permalink-one',
	  		title: 'this is the title',
				latitude: 43.4324,
				longitude: 42.43243,
				fake_full_address: 'fake_address',
				street_name: 'fdasf',
				street_number: '4324',
				state_id: State.first.id,
				county_id: County.first.id,
				city_id: City.first.id,
				pricing_unit: "UF"
	  	)
	end

  test "property is valid" do
  	assert @property.valid?
  end

  test "must have a permalink" do
  	@property.permalink = ""
  	assert_not @property.valid?
  end

  test "must have a title" do
  	@property.title = ""
  	assert_not @property.valid?
  end

	test "must have a state_id" do
		@property.state_id = ""
		assert_not @property.valid?
	end

	test "must have a ciy_id" do
		@property.city_id = ""
		assert_not @property.valid?
	end

	test "must have a county_id" do
		@property.county_id = ""
		assert_not @property.valid?
	end

	test "must have a street_name" do
		@property.street_name = ""
		assert_not @property.valid?
	end

	test "must have a street_number" do
		@property.street_number = ""
		assert_not @property.valid?
	end

	test "must have a fake_full_address" do
		@property.fake_full_address = ""
		assert_not @property.valid?
	end
	
	test "must have a latitude" do
		@property.latitude = ""
		assert_not @property.valid?
	end 

	test "must have a longitude" do
		@property.longitude = ""
		assert_not @property.valid?
	end

  test "can save property" do
  	assert_difference "Property.count", 1 do
  		@property.save
  	end
  end

  test "permalink must be unique" do
  	assert_difference "Property.count", 1 do
  		@property.save
  		@repeated_permalink_property.save
  	end
  end

	test "pricing unit must be correct" do
		@property.pricing_unit = "CX"
		assert_not @property.valid?

		@property.pricing_unit = "CLPD"
		assert_not @property.valid?

		@property.pricing_unit = "UF"
		assert @property.valid?
	end
end
