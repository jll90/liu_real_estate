# Preview all emails at http://localhost:3000/rails/mailers/marketing_mailer
class MarketingMailerPreview < ActionMailer::Preview

	def invitation
		MarketingMailer.invitation("maria@gmail.com", "Maria", "F")
	end

	def happy_new_year
		user = User.first
		MarketingMailer.happy_new_year(user, "maria@gmail.com", "Maria", "F", "NY")
	end
end
