# Preview all emails at http://localhost:3000/rails/mailers/quote_mailer
class QuoteMailerPreview < ActionMailer::Preview

	def deliver_quote
		@quote = Quote.new()
		QuoteMailer.deliver_quote(@quote)
	end
end
