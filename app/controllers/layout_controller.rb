class LayoutController < ApplicationController

	before_action :logged_in_user

	def index
		render :index
	end
end
