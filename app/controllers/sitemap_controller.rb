class SitemapController < ActionController::Base

	#MUST REFACTOR THIS VARIABLE
	VALID_STAGES = [4, 5, 6]

	def index
		headers['Content-Type'] = 'application/xml'
				@properties = Property.where(stage: VALID_STAGES)
				@articles = Article.all
		render :index 
	end
end
