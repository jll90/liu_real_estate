class PropertiesController < ApplicationController
  before_action :authenticate_request, except: [:show, :index, :suggestion, :featured_index, :just_listed_index, :code]
  before_action :set_property, only: [:show, :update, :update_layout, :edit, :destroy, :private_show]
  
  #DUP MAIN CONTROLLER
  VALID_STAGES = VALID_STAGES_FOR_SHOWCASING

  SHOW_PROPERTY_EXCEPT_PARAMS = PROPERTY_EXCEPT_PARAMS
  SHOW_PROPERTY_INCLUDE_PARAMS = [:selling_points, :photos]
  SHOW_USER_EXCEPT_PARAMS = USER_EXCEPT_PARAMS

  #these two lines are to be removed from index.html.erb after refactoring is done
  INDEX_PROPERTY_EXCEPT_PARAMS = PROPERTY_EXCEPT_PARAMS
  INDEX_PROPERTY_INCLUDE_PARAMS = [:photos]

  def index
    operation_type = params[:operation] 
    property_type = params[:propertyType]
    @properties = Property.includes(:county, :state, :city, :photos, :selling_points).where(operation_type: operation_type, stage: VALID_STAGES, property_type: property_type)
    render json: @properties.as_json(:include => [:photos, :city, :county, :state], :except => INDEX_PROPERTY_EXCEPT_PARAMS)
  end

  # def featured_index
  #   @showcased_properties = Property.includes(:photos).where(featured: true, stage: VALID_STAGES)
  #   render json: @showcased_properties.as_json(:include => [:photos, :city, :county, :state], :except => INDEX_PROPERTY_EXCEPT_PARAMS)
  # end

  def just_listed_index
    just_listed_properties = Property.limit(4).includes(:photos, :city, :county, :state).where(stage: VALID_STAGES).order('id DESC')
    render json: just_listed_properties.as_json(:include => [:photos, :city, :county, :state], :except => INDEX_PROPERTY_EXCEPT_PARAMS)
  end

	def code
		property_code = parse_property_code params[:code]
		property = Property.find(property_code)
		property_obj = { id: property.id, permalink: property.permalink }
		if property.is_listing_active?
			render json: property_obj
		else
			render json: {message: "Property not found", status: 404}, status: 404
		end
	end

  def show    
    if @property
        render json: @property.as_json(:include => [:photos, :city, :county, :state, :selling_points, user: {except: SHOW_USER_EXCEPT_PARAMS}], :except => INDEX_PROPERTY_EXCEPT_PARAMS)
    else
      	render json: {message: "Property could not be found", status: 404}.to_json, status: 404
    end
  end

  def suggestion    
    @properties = suggest_property params[:property_id], params[:operation_type]
    @uf = get_most_recent_uf
    render_as_json @properties, @uf.value
  end

  # GET /properties/new
  def new
    @property = Property.new
  end

  # GET /properties/1/edit
  def edit
    if @property
      render json: @property, status: 200
    else
      render json: {message: "Property could not be found", status: 404}.to_json, status: 404
    end
  end

	def private_show
    if @property
      render json: @property.as_json(:include => [:photos, :city, :county, :state, :selling_points, publications: {include: :platform}]), status: 200
    else
      render json: {message: "Property could not be found", status: 404}.to_json, status: 404
    end
	end

  # POST /properties
  def create
    @property = Property.new(property_params)

    if @property.save
      render json: {status: 200, message: "property created successfully"}, status: 200
    else
      render json: {status: 400, message: "bad request", errors: @property.errors}, status: 400
    end
  end

  #route created with the sole purpose of indexing properties when logged in
  def filtered_index
    filter = params[:filter]
    if filter == "default"
      @properties = Property.includes(:county, :city, :state, :photos, publications: [:platform]).where(stage: VALID_STAGES).order('id ASC')  
    end
    if filter == "all"
      @properties = Property.includes(:county, :city, :state, :photos, publications: [:platform]).order('id ASC')
    end
    render json: @properties.as_json(:include => [:city, :county, :state, :photos, publications: {include: :platform}])
  end

  # PATCH/PUT /properties/1
  def update
    if @property.update(property_params)
      render json: {message: "successfully updated",status: 200}, status: 200
    else
      render json: {message: @property.errors, status: 422}, status: 422
    end
  end

  def update_layout
    if @property.update(property_params)
      render json: {message: "successfully updated", status: 200}, status: 200
    else
      render json: {message: "could not update", status: 422}, status: 422
    end
  end

  # DELETE /properties/1
  def destroy
    @property.destroy
    redirect_to properties_url, notice: 'Property was successfully destroyed.'
  end

  private
    

    # Only allow a trusted parameter "white list" through.
    def property_params
      params.require(:property).permit(
        :property_type,
        :contact_name,
        :contact_phone_number,
        :contact_email,
        :contact_rut,
        :is_owner,
        :owner_name,
        :owner_phone_number,
        :owner_email,
        :owner_rut,
        :broker_name,
        :broker_phone_number,
        :broker_email,
        :broker_company,
        :inhouse_description,
        :public_description,
        :operation_type,
        :price,
        :stage,
        :small_surface,
        :big_surface,
        :bedrooms,
        :bathrooms,
        :single_space,
        :street_name,
        :street_number,
        :building_number,
        :fake_full_address,
        :storage_rooms,
        :parking_spots,
        :service_room,
        :rol_number,
        :permalink,
        :title,
        :approximate_location,
        :sales_lead_by,
        :number_of_units,
        :number_of_guests,
        :latitude, 
        :longitude,
        :featured,
        :state_id,
				:city_id,
				:county_id,
        :visiting_hrs,
        :publication_permissions,
        :published_on,
        :photo_layout,
        :uf,
        :right_to_key,
        :tradeable,
				:pricing_unit,
				:video_url,
        ownerships_attributes: [:client_id, :property_id],
      )
    end

    def suggest_property(id, operation_type)
      properties = Property.includes(:photos).where.not(id: id)
      suggested_properties = []
      suggested_properties << properties.where(stage: VALID_STAGES, operation_type: operation_type).sample
    end

    def render_as_json(properties, uf_value)
      render json: {
          properties: properties.as_json(:include => [:county, :city, :state, :photos, :selling_points], :except => PROPERTY_EXCEPT_PARAMS),
          uf_value: uf_value
        }, status: 200
    end

    def valid_route?
      if params[:operation_type] && params[:property_type]
        sql_params = {
          operation_type: (operation_type_to_id params[:operation_type]),
          property_type: (property_type_to_id params[:property_type])
        }
        return sql_params
      else
        return false
      end
    end
end
