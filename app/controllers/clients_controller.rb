class ClientsController < ApplicationController

	before_action :authenticate_request 

	def index
		clients = Client.all
		render json: clients.to_json
	end

	def create
		client = Client.new(client_params)

		if client.save
			render json: client.to_json, status: 200
		else
			render json: {message: "could not create client", status: 422, errors: client.errors}, status: 422
		end
	end

	def destroy

	end

	private

		def client_params
			params.require(:client).permit(
				:fullname,
				:email,
				:rut,
				:property_url,
				:phone_number,
				:description,
				:wechat,
				:whatsapp,
				:status
				)
		end
end
