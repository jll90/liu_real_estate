class PublicationsController < ApplicationController
	before_action :authenticate_request
	before_action :set_publication, only: [:update, :destroy]
	before_action :index_with_property_id?, only: [:index]

	def create
		@publication = Publication.create(publication_params)

		if @publication.save
			render json: @publication, status: 201
		else
			render json: {message: "failed to create", errors: @publication.errors}, status: 422
		end
	end

	def index
		publications = get_publications @property_id
		render json: publications.as_json(:include => [:platform]), status: 200
	end

	def destroy
		if @publication.destroy
			publications = get_publications nil #passes nil to not break function
			render json: {message: "success"}, status: 200
		else
			render json: {message: "error", errors: @publication.errors}, status: 422
		end
	end

	def update
		if @publication.touch(:updated_at)
			publications = get_publications
			render json: {message: "success"}, status: 200	
		else
			render json: {message: "error", errors: @publication.errors}, status: 422
		end
	end

	private

		def set_publication
			id = params[:id]
			@publication = Publication.find(id)			
		end

		def publication_params
			params.require(:publication).permit(
				:property_id,
				:platform_id,
				:days_to_update, 
				:url
				)
		end

		def get_publications (prop_id)
			if prop_id
				publications = Publication.where(property_id: prop_id).includes(:platform).order('updated_at ASC')
			else
				publications = Publication.all.includes(:platform).order('updated_at ASC')
			end
		end

		def index_with_property_id?
			@property_id = nil
			if params[:property_id]
				@property_id = params[:property_id]
			end
		end
end
