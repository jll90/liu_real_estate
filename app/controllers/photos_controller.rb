class PhotosController < ApplicationController

	before_action :authenticate_request
	before_action :set_property, except: [:destroy, :update]

	def new
		@photo = Photo.new
	end

	def create
		photo = @property.photos.build(photo_params)
		if photo.save
			render json: photo, status: 200
		else
			render json: {error: "error on create"}, status: 422
		end
	end

	def index
		photos = @property.photos
		if photos
			render json: photos, status: 200
		else
			render json: {error: "could not retrieve photos"}, status: 422
		end
	end

	def destroy
		photo = Photo.find(params[:id])
		if photo.destroy
			render json: photo, status: 200
		else
			render json: {message: "error destroying"}, status: 422
		end
	end

	def update
		photo = Photo.find(params[:id])

		if photo.update(photo_params)
			render json: {message: "success"}, status: 200
		else
			render json: {error: "error updating"}, status: 422
		end
	end

	def update_all
		photos_as_params = update_photos_params 
		photos_as_params.each do |photo_params|
			photo = Photo.find(photo_params[:id])
			photo.update(photo_params)
		end
		render json: {message: "success"}, status: 200
	end

	private

		def photo_params
			params.require(:photo).permit(
				:url,
				:order
				)
		end

		def update_photos_params
			params.require(:_json).map do |p|
				ActionController::Parameters.new(p).permit(
					:id,
					:order
				)
			end
		end
end
