class AppointmentsController < ApplicationController

	before_action :logged_in_user, only: [:index]

	def index

	end

	def new
			
	end

	def create
		@appointment = Appointment.new(appointment_params)
		if @appointment.save
			# @appointment.send_appointment_email
			render json: {message: "success", status: 200}.to_json, status: 200
		else
			render json: {message: "failed validation", status: 422, errors: @appointment.errors}.to_json, status: 422
		end
	end

	private
		def appointment_params
			params.require(:appointment).permit(:fullname,
										:email,
										:phone_number,
										:message)
		end
end
