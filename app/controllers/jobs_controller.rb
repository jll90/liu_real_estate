class JobsController < ApplicationController

	before_action :logged_in_user

	def twitter
		@properties = Property.where(stage: [4,5,6])

		@properties.shuffle.each do |property|
			number_of_mins = rand(1..720) #12 hours
			TwitterPostJob.set(wait: 1.minutes).perform_later(property)
		end
		render json: {message: "All posts were successfully queued", status: 200}, status: 200
	end
end
