class IndicesController < ApplicationController

	skip_before_action :authenticate_request, only: [:index]

	def create
		@index = Index.new(index_params)
		if @index.save
			render json: {message: "Successfully saved.", status: 200}, status: 200
		else
			render json: {message: "Could not save", status: 422, errors: @index.errors.as_json }, status: 422
		end
	end

	def index
		@uf = get_most_recent_uf
		render json: @uf
	end

	private

		def index_params
			params.require(:index).permit(
				:value,
				:reading_time,
				:name
			)
		end

end