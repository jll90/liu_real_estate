class ApplicationController < ActionController::API
  include ApplicationHelper
  include DescriptionHelper
  include SessionsHelper
  include IndicesHelper
	include PropertiesHelper

	before_action :authenticate_request
	attr_reader :current_user

  private
		def authenticate_request 
			@current_user = AuthorizeApiRequest.call(request.headers).result 
			render json: { error: 'Not Authorized' }, 
			status: 401 unless @current_user 
		end
  	#supports id or permalink
    def set_property
      case params[:controller]
        when "properties"
          id = params[:id]
        when "selling_points"
          id = params[:property_id]
        when "photos"
          id = params[:property_id]
      end
      if ( id =~ /^\d+$/)
        @property = Property.includes(:selling_points, :photos).find(id)
      else
        @property = Property.includes(:selling_points, :photos).find_by_permalink(id)
      end

      if params[:controller] == "properties" && params[:action] == "show"
        @property = (VALID_STAGES_FOR_SHOWCASING.include? @property.stage) ? @property : nil
      end     

			if params[:controller] == "properties" && params[:action] == "private_show"
				@property = Property.includes(
					:selling_points,
					:photos,
					:state,
					:city,
					:county,
					publications: [:platform],
				).find(id)
			end 
    end
end
