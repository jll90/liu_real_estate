class MailerController < ApplicationController

	before_action :authenticate_request, except: [:get_to_know_us]
	#otherwise mailer.layout is called
	#this line is needed
	#layout "application"

	BLACKLIST = MAILER_BLACKLIST;
	CLIENT_EMAIL_LIST = Property.all.pluck(:contact_email)
	CLIENT_EMAIL_LIST_2 = Client.all.pluck(:email)

	def send_welcome_email
		markdown_parser = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
		to_email = params[:mailer][:email]
		markdown_message = params[:mailer][:message]
		message = markdown_parser.render markdown_message #must filter
		MarketingMailer.welcome(to_email, message, current_user).deliver_now
		render json: {message: "success", status: 200}, status: 200
	end

	def get_to_know_us
		email = params[:mailer][:to_email]
		client_name = params[:mailer][:client_name]
		gender = params[:mailer][:gender]
				#from_email, to_email, client_name
				send_email(email, client_name, gender)
				render json: {message: "Mail sent successfully.", status: 200}.to_json, status: 200
	end

	def send_multiple
		#fields #name #gender #email_addr #email_type
		data = JSON.parse(params[:csv_data])
		
		data.each_with_index do |row, i|
			email = row[2]
			email_type = row[3]
			gender = row[1]
			client_name = row[0]

			puts email
			puts email_type
			puts gender
			puts client_name

			wait_time = (rand(10..15) * i).seconds

			send_email(email, client_name, gender, email_type, wait_time)
		end
	end

	private

		def get_to_know_us_params
			params.require(:mailer).permit(				
				:to_email,
				:client_name,
				:gender,
				:email_type
			)
		end

		def send_email(to_email, client_name, gender)
			if !(BLACKLIST.include? to_email) && !(CLIENT_EMAIL_LIST.include? to_email) && !(CLIENT_EMAIL_LIST_2.include? to_email)
#				if email_type == "E"
#					MarketingMailer.exchange(current_user, to_email, client_name, gender, email_type).deliver_later!(wait: offset)
#				else
#					if offset.nil?
#						MarketingMailer.invitation(current_user, to_email, client_name, gender, email_type).deliver_now
#					else
#						MarketingMailer.invitation(current_user, to_email, client_name, gender, email_type).deliver_later!(wait: offset)
#					end
#				end

				
				MarketingMailer.invitation(to_email, client_name, gender).deliver_now
				
			end
		end
end
