class PlacesController < ApplicationController

	def index
		states = State.all
    cities = City.all
    counties = County.all

    render json: { 
			states: states,
      cities: cities,
      counties: counties
		} 
  end

end
