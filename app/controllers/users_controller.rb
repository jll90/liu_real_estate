class UsersController < ApplicationController

	skip_before_action :authenticate_request

	def create
		@user = User.new(user_params)
		render json: {}, status: 200
		#if @user.save
		#	@user.send_activation_email
		#	redirect_to root_url, t('alerts.notice.account.see_activation_email'), "notice"
	#	else
	#		render :new
	#	end
	end

	private
		def user_params
			params.require(:user).permit(:firstname, :lastname,
										:email,
										:password,
										:password_confirmation)
		end

end
