class SellingPointsController < ApplicationController

	before_action :logged_in_user
	before_action :set_property

	def new
		@selling_point = @property.selling_points.build
	end

	def create
		@selling_point = @property.selling_points.build(selling_point_params)
		if @selling_point.save
			redirect_to new_property_selling_point_path, notice: "Ventaja agregada correctamente"
		else
			render :new
		end
	end

	def edit

	end

	def update

	end

	
	private
		def set_selling_point
			
		end

		def selling_point_params
			params.require(:selling_point).permit(
					:category,
					:description
				)
		end
end
