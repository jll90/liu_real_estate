class ArticlesController < ApplicationController

	before_action :authenticate_request, except: [:show, :index]
	before_action :set_article, only: [:show]

	def index
		lang = params[:lang]
		@articles = Article.where(language: lang)
		render json: @articles.to_json
	end

	def new

	end

	def create
		@article = Article.new(article_params)

		if @article.save
			render json: @article.to_json, status: 200
		else
			render json: {message: "could not create article", status: 422, errors: @article.errors}.to_json, status: 422
		end
	end

	def update

	end

	def edit

	end

	def destroy

	end

	def show
		if @article
			render json: @article.to_json
		else
			render json: {message: "Article could not be found", status: 404}.to_json, status: 404
		end
	end

	private

		def article_params
			params.require(:article).permit(
				:permalink,
				:title,
				:content,
				:image_url
				)
		end

		def set_article
			id = params[:id]
			if ( id =~ /^\d+$/)
		       @article = Article.find(id)
		    else
		       @article = Article.find_by_permalink(id)
		    end
		end

end
