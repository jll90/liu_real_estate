class Publication < ApplicationRecord
	belongs_to :platform
	validates :property_id, presence: true
	validates :platform_id, presence: true

	validates :days_to_update, presence: true

	validates_numericality_of [:property_id, :platform_id, :days_to_update], :greater_than_or_equal_to => 1

	validates_uniqueness_of :property_id, scope: :platform_id
end
