class Platform < ApplicationRecord
	has_many :publications
	validates :website, presence: true
	validates :name, presence: true
	validates :days_to_update, presence: true

	validates_numericality_of :days_to_update, :greater_than_or_equal_to => 1
end
