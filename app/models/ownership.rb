class Ownership < ApplicationRecord
	belongs_to :property
	belongs_to :client	

	# validates :property_id, presence: true
	validates :client_id, presence: true

	validates_uniqueness_of :client_id, scope: :property_id
end
