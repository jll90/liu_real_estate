class Article < ApplicationRecord

	validates :title, presence: true
	validates :permalink, presence: true
	validates :content, presence: true

	has_permalink :permalink
end
