class Photo < ApplicationRecord
	belongs_to :property
	validates :url, presence: true

	default_scope { order(order: :asc)}
end