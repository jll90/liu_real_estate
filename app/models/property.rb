class Property < ApplicationRecord

	#see file constants.rb
	has_permalink :permalink
	
	has_many :photos
	has_many :selling_points
	has_many :ownerships
	has_many :clients, through: :ownerships
	has_many :publications
	belongs_to :user

	accepts_nested_attributes_for :ownerships

	validates :permalink, uniqueness: {case_sensitive: false}, presence: true

	VALID_PRICING_UNIT_REGEX = /\A(CLP|UF)\z/i

	validates :title, presence: true
	validates :state_id, presence: true
	validates :city_id, presence: true
	validates :county_id, presence: true
	validates :street_name, presence: true
	validates :street_number, presence: true
	validates :latitude, presence: true
	validates :longitude, presence: true
	validates :fake_full_address, presence: true

	validates :pricing_unit, presence: true, format: {with: VALID_PRICING_UNIT_REGEX}

  belongs_to :city
  belongs_to :county
  belongs_to :state

	def fake_full_address_without_region
		address = self.fake_full_address
		params = address.split(",")

		#prevents city from being removed in case region is not present
		if params.length == 3
			params = params - [params.last]
		end

		address = "#{params.join(",")}, #{self.comune}, #{self.city}"
	end

	def generate_twitter_post
		twit = "#{as_hashtag(operation_as_verb.delete(' '))}"
		twit += " "
		twit += "#{as_hashtag(property_type_name).delete(' ')}"
		twit += " "
		twit += "#{distribution}"
		twit += " "
		twit += "#{surface_area}"
		twit += " en "
		twit += "#{as_hashtag(self.comune.camelcase.delete(' '))}"
		twit += ", #{self.city.titleize}"
		twit += "\n\n"
		twit += "#{short_uri}"
	end

	def short_uri
		uri = "www.liu-propiedades.com/propiedades/#{self.id}"
	end

	def long_uri
		uri = "www.liu-propiedades.com/propiedades/#{self.permalink}"
	end

	def is_listing_active?
		VALID_STAGES_FOR_SHOWCASING.include? self.stage 
	end

	private

		def operation_as_verb
			op = self.operation_type

			sale = 0
			rent = 1
			season = 2
			
			if op == sale
				op_hash = "se vende"
			end

			if op == rent
				op_hash = "se arrienda"
			end

			if op == season
				op_hash = "se arrienda por dia"
			end

			return op_hash
		end

		def property_type_name
			p_id = self.property_type

			p_name = property_name_translator p_id
		end

		def distribution
			dist = "#{self.bedrooms}D|#{self.bathrooms}B"
		end

		def surface_area
			house = 0
			apartment = 1
			office = 2
			commercial = 3 
			industrial = 4
			storage = 5
			site = 6
			land = 7
			parking = 8
			
			if property_type == house
				surface = "#{self.built_surface}m² construidos / #{self.terrain_surface}m² terreno"
			end

			if property_type == apartment
				surface = "#{self.rentable_surface}m² construidos / #{self.usable_surface}m² útiles"
			end

			if property_type == site
				surface = "#{self.terrain_surface}m² terreno"
			end

			return surface
		end

end
