class City < ApplicationRecord
	belongs_to :state
	has_many :counties
  has_many :properties
end
