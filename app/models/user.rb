class User < ApplicationRecord


    
    has_many :properties
    # has_many :friend_requests, dependent: :destroy
    # has_many :pending_friends, through: :friend_requests, source: :friend

    # has_many :friendships, dependent: :destroy
    # has_many :friends, through: :friendships

    # has_many :language_threads
    # has_many :travel_threads
    # has_many :replies
    # has_many :notifications
    # has_many :stories
    # has_many :subscriptions
    # has_many :pictures
    # has_many :profile_pictures
    # has_many :cover_pictures

    # has_many :user_languages
    # has_many :languages, through: :user_languages

    # has_many :events_participant, foreign_key: "participant_id"
    # has_many :events, through: :events_participant

    # has_many :user_places
    # has_many :places, through: :user_places

    # has_many :user_conversations
    # has_many :conversations, through: :user_conversations
    # has_many :messages

    # has_many :invitations

    # has_one :hometown, class_name: "Place", foreign_key: :id, primary_key: :hometown_id
    # has_one :current_residence, class_name: "Place", foreign_key: :id, primary_key: :current_residence_id
    # has_one :current_profile_picture, class_name: "ProfilePicture", foreign_key: :id, primary_key: :profile_picture_id
    # has_one :current_cover_picture, class_name: "CoverPicture", foreign_key: :id, primary_key: :cover_picture_id

    # has_many :unread_messages, class_name: "MessageFlag", foreign_key: :user_id

	attr_accessor :remember_token, :activation_token, :reset_token, :original_password
	before_save :downcase_email
	before_create :create_activation_digest

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	# VALID_GENDER_REGEX = /\A[FMO]{1}\z/
 #    VALID_LANG_PREF_REGEX = /\A(en|es|ja)\z/
 #    VALID_PRIVACY_REGEX = /\A[FT]{1}\z/

	validates :firstname, presence: {message: :presence}, length: {maximum: 20, message: :length}
	validates :lastname, presence: {message: :presence}, length: {maximum: 32, message: :length}
	validates :email, presence: {message: :presence}, length: {maximum: 64, message: :length},
				format: { with: VALID_EMAIL_REGEX, message: :valid }, uniqueness: {case_sensitive: false, message: :unique}
	has_secure_password
	validates :password, presence: {message: :presence}, length: {minimum: 6, message: :length}, allow_nil: true

    # before_save :update_residence_hometown_count, on: :update

    def firstname
        if self[:firstname]
            firstname = self[:firstname].split(" ")
            firstname.map! {|n| n.capitalize}
            return firstname.join(" ")
        end
    end

    def lastname
        if self[:lastname]
            lastname = self[:lastname].split(" ")
            lastname.map! {|n| n.capitalize}
            return lastname.join(" ")
        end
    end

	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
    end

    def User.new_token
    	SecureRandom.urlsafe_base64
    end

    def remember
    	self.remember_token = User.new_token
    	update_attribute(:remember_digest, User.digest(remember_token))
    end

    def forget
    	update_attribute(:remember_digest, nil)
    end

    def authenticated?(attribute, token)
    	digest = send("#{attribute}_digest")
    	return false if digest.nil?
    	BCrypt::Password.new(digest).is_password?(token)
    end

    def activate
    	update_attribute(:activated, true)
    	update_attribute(:activated_at, Time.zone.now)
    end

    def send_activation_email
    	UserMailer.account_activation(self).deliver_now
    end

      # Sets the password reset attributes.
    def create_reset_digest
        self.reset_token = User.new_token
        update_attribute(:reset_digest,  User.digest(reset_token))
        update_attribute(:reset_sent_at, Time.zone.now)
    end

    # Sends password reset email.
    def send_password_reset_email
        UserMailer.password_reset(self).deliver_now
    end

    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end

    def fullname
        fullname = self.firstname + " " + self.lastname
    end

    def update_email(updated_email)
        self.update(email: updated_email)
    end

    private
    	def downcase_email
    		self.email = email.downcase
    	end

    	def create_activation_digest
    		self.activation_token = User.new_token
    		self.activation_digest = User.digest(activation_token)
    	end
end
