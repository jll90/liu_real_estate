class Client < ApplicationRecord
	has_many :ownerships
	has_many :properties, through: :ownerships

	validates :fullname, presence: true

	#cannot be blank
	validates_numericality_of :status, :greater_than_or_equal_to => 0
end