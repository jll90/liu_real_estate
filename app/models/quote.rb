class Quote < ApplicationRecord
	validates :fullname, presence: true
	validates :email, presence: true
	validates :message, presence: true

	def send_quote_email
		QuoteMailer.deliver_quote(self).deliver_now
	end
end