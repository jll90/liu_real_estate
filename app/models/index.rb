class Index < ApplicationRecord
	validates :name, presence: true
	validates :reading_time, presence: true
	validates :value, presence: true
end