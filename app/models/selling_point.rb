class SellingPoint < ApplicationRecord
	belongs_to :property

	validates :description, presence: true
end
