class TwitterPostJob < ApplicationJob
  queue_as :default

  def perform(property)
    post = property.generate_twitter_post
    $twitter.update(post)
  end
end
