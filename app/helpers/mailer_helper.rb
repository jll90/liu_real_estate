module MailerHelper

	def set_mailer_message(email_type)

		if email_type == "D"
			message = MAILER_MESSAGES[MAILER_DEFAULT_MESSAGE]
		end

		if email_type == "R"
			message = MAILER_MESSAGES[MAILER_RENT_MESSAGE]
		end

		if email_type == "S"
			message = MAILER_MESSAGES[MAILER_SALE_MESSAGE]
		end

		return message
	end
end

