module ApplicationHelper

	def operation_type_to_id(string)
		id = case string
			when "sale" then 0
			when "rent" then 1
			when "season" then 2
		end
		return id
	end

	def property_type_to_id(string)
		id = case string
			when "house" then 0
			when "apartment" then 1
			when "office" then 2
			when "commercial" then 3
			when "industrial" then 4
			when "storage" then 5
			when "site" then 6
			when "land" then 7
			when "parking" then 8
		end
		return id
	end

	def operation_name_translator(id)
		operation_name = case id
			when 0 then "venta"
			when 1 then "arriendo"
			when 2 then "temporada"
		end
		return operation_name.capitalize
	end

	def selling_point_category_translator(id)
		category_name = case id
			when 0 then "Vivienda"
			when 1 then "Instalaciones"
			when 2 then "Ubicación"
			when 3 then "Terreno"
		end
		return category_name.capitalize
	end

	def title_generator(obj) 
		default_title = "LIU Propiedades"
		title = default_title
		path = request.path
		
		if path == "/"
			title = index_title
		end

		if path == "/propiedades"
			title = properties_title
		end

		if path == "/blog"
			title = articles_title
		end

		if path.include? "/blog/"
			if obj.class.name == "Article"
				title = article_title(obj)
			end
		end

		if path.include? "/propiedades/"
			if obj.class.name == "Property"
				title = property_title(obj)
			end
		end

		if path.include? "solicitudes/nuevo"
			title = "Te ayudamos a encontrar la propiedad de tus sueños"
		end	


		#appends | LIU Propiedades at the end
		if title != default_title
			title = "#{title} | LIU Propiedades"
		end

		return title
	end

	def image_generator(obj)
		default_image_url = "http://i.imgur.com/GlCjI24.png"
		image_url = default_image_url

		if obj.class.name == "Property"
			if obj.photos.length > 0
				image_url = obj.photos.first.url
			end
		end

		if obj.class.name == "Article"
			image_url = obj.image_url
		end
		return image_url
	end

	def inside_cms?
		c_name = controller.controller_name
		a_name = controller.action_name

		if (c_name == "properties" && a_name == "new") ||
			(c_name == "photos") ||
			(c_name == "selling_points") ||
			(c_name == "sessions" && a_name == "new")
			return true
		end

		return false
	end

	private 
		def index_title
			title = "Compra, Vende y Arrienda Propiedades"
		end

		def articles_title
			title = "Indice de Articulos"
		end

		def article_title(a)
			title = a.title.titleize
		end

		def properties_title
			default_title = "Propiedades en el mercado"
			fullpath = request.fullpath
			path = request.path
			if fullpath == path
				title = default_title
			end

			query_params = fullpath.split("?")

			query_params.each do |param|
				if param.include?("compra")
					title = "Propiedades en venta"
				end

				if param.include?("arriendo")
					title = "Propiedades en arriendo"
				end

				if param.include?("temporada")
					title = "Arriendos por temporada o por día"
				end
			end

			return title
		end
		def property_title(p)
			p_type = p.property_type

			##Operation type
			title = "Se"
			operation_verb = case p.operation_type
				when 0 then "vende"
				when 1 then "arrienda"
				when 2 then "arrienda por temporada"
			end
			title = "#{title} #{operation_verb}"

			bedroom_singular = "dormitorio"
			bathroom_singular = "baño"
			
			##Property type			
			if p_type == 3 && p.right_to_key == true
				title = "#{title} Derecho de Llave"
			else 
				title = "#{title} #{(property_name_translator p.property_type)}"
			end

			# if p_type == 0 || p_type == 1
			# 	title = "#{title} de #{p.bedrooms}D|#{p.bathrooms}B"
			# end

			if p_type == 0 || p_type == 1

				if p.single_space == true
					title = "#{title} de 1 ambiente"
				else
					number_of_bedrooms = p.bedrooms > 1 ? bedroom_singular.pluralize(:es) : bedroom_singular.singularize(:es)
					title = "#{title} de #{p.bedrooms} #{number_of_bedrooms}"
				end
				
				number_of_bathrooms = p.bathrooms > 1 ? bathroom_singular.pluralize(:es) : bathroom_singular.singularize(:es)
				title = "#{title} y #{p.bathrooms} #{number_of_bathrooms}"

				if p.parking_spots && p.storage_rooms
					if p.parking_spots > 0 && p.storage_rooms > 0
						title = "#{title} con estacionamiento y bodega"
					end
				else
					if p.parking_spots
						if p.parking_spots > 0
							title = "#{title} con estacionamiento"
						end
					end

					if p.storage_rooms
						if p.storage_rooms > 0
							title = "#{title} con bodega"
						end
					end
				end
			end
			if p_type == 6
				title = "#{title} de #{p.terrain_surface}m²"
			end
			if p_type == 3
				title = "#{title} de #{p.rentable_surface}m²"
			end
			
			title = "#{title} en"
			title = "#{title} #{p.fake_full_address_without_region}"
		end	
end
