module IndicesHelper

	def get_most_recent_uf
		uf = Index.where('reading_time BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).first

		if uf.nil?
			uf = Index.last
		end

		return uf
	end

end