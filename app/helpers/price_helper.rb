module PriceHelper

	def format_price(price)
		price_shortened_form(price)
	end

	def format_uf_price(uf_price)
		uf_price_shortened_form(uf_price)
	end

	private

		def price_shortened_form(price)
			if price > 999999
				short_price = price/1000000.0
				price_string = "#{short_price} millones"
			else
				short_price = price/1000.0
				price_string = "#{short_price} mil"
			end

			return price_string
		end

		def uf_price_shortened_form(uf_price)
			return "#{uf_price} UF"
		end
end