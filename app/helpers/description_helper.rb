module DescriptionHelper

	def description_generator(obj)
		default_description = "Compra, arriendo y venta de Propiedades. Descubre las mejores propiedades de Santiago y el resto de Chile. 
		Somos expertos en corretaje e inversiones inmobiliarias y ofrecemos servicios de administración de propiedades incluyendo estadias cortas. 
		Cobramos las tasas más competitivas del mercado entregando un servicio integro y de calidad."

		path = request.path

		if path == "/"
			description = default_description
		end

		if path == "/blog"
			description = "Blog relacionado al mundo del corretaje de propiedades, inversiones inmobiliarias y tendencias del mercado."
		end

		if path.include? "/blog/"
			description = article_description(obj)
		end

		if path.include? "/venta/"
			description = "Compre la propiedad de sus sueños o invierta en el mercado inmobiliario. A la venta exclusiva seleccion de departamentos y casas, entre otros.
			Lo asesoraremos en cada paso de la compra, asegurándole rentabilidad y plusvalía a largo plazo."
		end

		if path.include? "/arriendo/"
			description = "Arriende un departamento o una casa o local comercial para su negocio."
		end

		if path.include? "/temporada/"
			description = "Descubra las propiedades que tenemos para arrendar y disfrutar unos días de vacaciones. Estadias cortas y arriendos por semanas y meses."
		end

		if path.include? "/propiedades/"
			if obj.class.name == "Property"
				description = property_description(obj)
			end
		end

		if path.include? "solicitudes/nuevo"
			description = "Si está buscando un tipo de propiedad en particular, puede hacernos un encargo 
			y encontraremos la propiedad de sus sueños o la siguiente gran inversión para usted. Contamos con una base de datos y 
			una red extensa de agentes asociados."
		end

		return description
	end

	private

		def property_description(p)
			description = ""
			description = "#{p.public_description.split(/[\r\n]+/).first}"
			if p.operation_type == 0 #0 stands for sale
				description = "#{description} A sólo #{format_uf_price (p.uf)} -"
			else
				if !p.price.nil?
					description = "#{description} A sólo #{format_price (p.price)} -"
				else
					description = "#{description} A sólo #{format_uf_price (p.uf)} -"
				end
			end
			description = "#{description} #{p.approximate_location}"
		end

		def article_description(a)
			description = a.meta_description
		end
end
