import axios from 'axios';
import { frontEndRouter} from '../routing/router.jsx';

import { jsonSnakeCaseToCamelCase, jsonCamelCaseToSnakeCase } from '../tools/object_manipulation.jsx';

import { getToken, destroyToken } from '../providers/authentication_provider.jsx';  

var jsonProvider = {};

function authorizationHeaders() {
	return { Authorization: getToken() };
}

function setup(){
	axios.interceptors.response.use(function (response) {
		response.data = jsonSnakeCaseToCamelCase(response.data);
		return Promise.resolve(response);
	}, function (error){
		const response = error.response;
		if ( response.status == 401 ){
			const pathname = frontEndRouter('loginPath');
      destroyToken();
			window.location.pathname = pathname;
		} else {
			return Promise.reject(error);
		}
	})

  axios.interceptors.request.use(function (config){
    config.data = jsonCamelCaseToSnakeCase(config.data);
		config.headers = authorizationHeaders();
    return config;
	})
}

function isValidMethod(method){
	var validMethodsRegexp = /GET|POST|PATCH|PUT|DELETE/gi;
	return validMethodsRegexp.test(method);
}

function buildConfig(c, method){

	if (!c.hasOwnProperty("url")){
		throw("A url is required");
	}

	if (!isValidMethod(method)){
		throw("A valid method is required");
	}

	return {
		url: c.url,
		data: c.hasOwnProperty("data") ? c.data : null,
		successFn: c.hasOwnProperty("successFn") ? c.successFn : undefined,
		errorFn: c.hasOwnProperty("errorFn") ? c.errorFn : undefined,
    alwaysFn: c.hasOwnProperty("alwaysFn") ? c.alwaysFn : undefined,
		postProcessor: c.hasOwnProperty("postProcessor") ? c.postProcessor : function (d) { return d}, 
		method: method,
		headers: c.hasOwnProperty("headers") ? c.headers : undefined
	}
}

function get(config){
	const ajaxCallConfig = buildConfig(config, "GET");

	ajaxCall(ajaxCallConfig);	
}

function patch(config){
	const ajaxCallConfig = buildConfig(config, "PATCH");

	ajaxCall(ajaxCallConfig);
}

function post(config){
	const ajaxCallConfig = buildConfig(config, "POST");

	ajaxCall(ajaxCallConfig);
}

//del acts as alias for delete which is a reserved word
function del(config){	
	const ajaxCallConfig = buildConfig(config, "DELETE");

	ajaxCall(ajaxCallConfig);
}

function ajaxCall(config){
	const { url, method, successFn, errorFn, alwaysFn, data, headers, postProcessor } = config;

	axios({
		url: url,
		method: method,
		headers: headers,
		data: data
	})
	.then(function (response){
    if (alwaysFn !== undefined){
      alwaysFn();
		}
		if (successFn !== undefined){
			successFn(postProcessor(response.data));	
		}
	})
	.catch(function (error){
		console.log(error);
    if (alwaysFn !== undefined){
      alwaysFn();
		}
		if (errorFn !== undefined){
			errorFn();	
		}
    
	});
}

setup();

jsonProvider = {
	get: get,
	patch: patch,	
	del: del,
	post: post
}

export default jsonProvider;
