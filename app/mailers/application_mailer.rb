class ApplicationMailer < ActionMailer::Base
  default from: 'server@liu-propiedades.com'
  layout 'mailer'
end
