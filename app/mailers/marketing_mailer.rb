class MarketingMailer < ApplicationMailer

	#the following doesn't work
	#include MailerHelper
	helper MailerHelper

	def happy_new_year(user, to_email, client_name, gender, email_type)
		subject = "Feliz Año Nuevo 2018!"
		from = "Equipo LIU Propiedades <contacto@liu-propiedades.com>"
		@client_name = client_name
		@gender = gender
		mail to: to_email, subject: subject, from: from 
	end

	def invitation(to_email, client_name, gender)
		@client_email = to_email
		@client_name = client_name
		@gender = gender
		
		subject = "Su propiedad al mejor precio"

		@from_string = "Penthouse Propiedades <contacto@penthouse.solutions>"
		mail to: @client_email, subject: subject, from: @from_string
	end

	def exchange(user, to_email, client_name, gender, email_type)
		@from_name = "Jesús Luongo Lizana"
		@from_email = "jesus@liu-propiedades.com"
		@addresse_email = to_email
		@addresse_name = client_name
		@gender = gender

		subject = "Buscamos propiedad en Zapallar"

		@from_string = "#{@from_name} <#{@from_email}>"
		mail to: to_email, subject: subject, from: @from_string
	end

	def welcome(email, message, user)
		subject = "Su propiedad ya está en el mercado"
		@from_string = "#{user.fullname} <#{user.email}>"
		@user = user
		@message = message
		mail to: email, subject: subject, from: @from_string
	end
end
