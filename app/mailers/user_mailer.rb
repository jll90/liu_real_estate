class UserMailer < ApplicationMailer

 def account_activation(user)
    @user = user
    mail to: user.email, subject: "Activar Cuenta"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Reestablecer Contraseña"
  end
end
