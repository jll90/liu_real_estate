class RemoveSurfaceColumns < ActiveRecord::Migration[5.0]
  def change
    remove_column :properties, :usable_surface
    remove_column :properties, :built_surface
    remove_column :properties, :rentable_surface
    remove_column :properties, :terrain_surface
  end
end
