class IndexValueAsFloat < ActiveRecord::Migration[5.0]
  def change
  	change_column :indices, :value, :float
  end
end
