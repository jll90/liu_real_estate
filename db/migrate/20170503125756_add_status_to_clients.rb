class AddStatusToClients < ActiveRecord::Migration[5.0]
  def change
  	add_column :clients, :status, :integer, index: true, default: 0
  end
end
