class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :fullname
      t.string :email
      t.string :rut
      t.string :property_url
      t.string :phone_number
      t.timestamps
    end
  end
end
