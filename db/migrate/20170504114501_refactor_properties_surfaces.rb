class RefactorPropertiesSurfaces < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :small_surface, :integer
  	add_column :properties, :big_surface, :integer
  end
end
