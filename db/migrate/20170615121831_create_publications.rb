class CreatePublications < ActiveRecord::Migration[5.0]
  def change
    create_table :publications do |t|
      t.references :property, index: true, foreign_key: true
      t.references :platform, index: true, foreign_key: true
      t.integer :days_to_update
      t.timestamps
    end

    add_index :publications, [:property_id, :platform_id], unique: true
  end
end
