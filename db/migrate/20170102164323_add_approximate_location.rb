class AddApproximateLocation < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :approximate_location, :string
  end
end
