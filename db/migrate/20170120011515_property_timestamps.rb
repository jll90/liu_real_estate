class PropertyTimestamps < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :updated_at, :timestamp
  	add_column :properties, :created_at, :timestamp
  end
end
