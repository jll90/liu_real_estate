class RemoveClientColumnFromProperties < ActiveRecord::Migration[5.0]
  def change
  	remove_column :properties, :client_id, :integer
  end
end
