class CreateOwnerships < ActiveRecord::Migration[5.0]
  def change
    create_table :ownerships do |t|
      t.references :client, foreign_key: true, index: true
      t.references :property, foreign_key: true, index: true
      t.timestamps
    end

    add_index :ownerships, [:client_id, :property_id], unique: true
  end
end
