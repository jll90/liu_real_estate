class PhotoAddCollage < ActiveRecord::Migration[5.0]
  def change
  	add_column :photos, :collage, :integer, index: true
  	add_index :photos, [:property_id, :collage], unique: true
  end
end
