class PropertyTradeable < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :tradeable, :boolean, default: false
  end
end
