class AddSalesLeadToProperty < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :sales_lead_by, :string
  end
end
