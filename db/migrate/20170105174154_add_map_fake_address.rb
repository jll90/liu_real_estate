class AddMapFakeAddress < ActiveRecord::Migration[5.0]
  def change
  	#used with google maps to set marker
  	add_column :properties, :fake_full_address, :string
  end
end
