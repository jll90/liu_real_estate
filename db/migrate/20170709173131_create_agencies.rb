class CreateAgencies < ActiveRecord::Migration[5.0]
  def change
    create_table :agencies do |t|
	    t.string :name
			t.string :description
			t.string :profile_img
			t.string :tax_id
      t.timestamps
    end
  end
end
