class AddUrlToPublications < ActiveRecord::Migration[5.0]
  def change
  	add_column :publications, :url, :string
  end
end
