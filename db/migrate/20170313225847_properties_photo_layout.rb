class PropertiesPhotoLayout < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :photo_layout, :string, default: '{"id": -1, "photos": []}'
  end
end
