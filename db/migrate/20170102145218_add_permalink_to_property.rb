class AddPermalinkToProperty < ActiveRecord::Migration
  # def self.up
  #   add_column :properties, :permalink, :string
  #   add_index :properties, :permalink
  # end
  # def self.down
  #   remove_column :properties, :permalink
  # end

  # The above migration was generated after running the command
  # on the command line
  # rails g has_permalink Property
  # However, the permalink column had already been created
  def self.up
  	add_index :properties, :permalink
  end


  def self.down

  end
end