class AddNeighborhoodAndComission < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :neighborhood, :string
  	add_column :properties, :no_comission, :boolean, default: :false
  end
end
