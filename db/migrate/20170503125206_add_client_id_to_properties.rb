class AddClientIdToProperties < ActiveRecord::Migration[5.0]
  def change
  	add_reference :properties, :client, index: true, foreign_key: true
  end
end
