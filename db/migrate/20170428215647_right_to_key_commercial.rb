class RightToKeyCommercial < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :right_to_key, :boolean, default: false
  end
end
