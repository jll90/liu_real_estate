class OperationTypeStageIndex < ActiveRecord::Migration[5.0]
  def change
  	add_index :properties, [:operation_type, :stage]
  	add_index :properties, [:operation_type, :stage, :state_id]
  	add_index :properties, [:property_type, :stage]
  end
end
