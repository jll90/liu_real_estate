class PropertiesNumberGuests < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :number_of_guests, :integer, index: true
  end
end
