class CreateVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :visits do |t|
      t.references :property, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true
      t.timestamp :time
      t.timestamps
    end
  end
end
