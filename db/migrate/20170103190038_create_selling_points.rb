class CreateSellingPoints < ActiveRecord::Migration[5.0]
  def change
    create_table :selling_points do |t|
      t.references :property, index: true, foreign_key: true
      t.integer :category, index: true
      t.string :description
      t.timestamps
    end
  end
end
