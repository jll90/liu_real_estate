class AddUfToProperties < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :uf, :integer, index: true
  	remove_column :properties, :currency
  end
end
