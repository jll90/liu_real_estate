class CreatePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :photos do |t|
      t.references :property, index: true, foreign_key: true
      t.string :url
      t.boolean :showcased, default: :false
      t.timestamps
    end
  end
end
