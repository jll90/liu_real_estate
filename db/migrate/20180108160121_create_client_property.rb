class CreateClientProperty < ActiveRecord::Migration[5.0]
  def change
    create_table :client_property do |t|
			t.references :client, foreign_key: true, index: true
			t.references :property, foreign_key: true, index: true
			t.string :role
    end

		add_index :client_property, [:client_id, :property_id]
		add_index :client_property, [:property_id, :client_id]
  end

end
