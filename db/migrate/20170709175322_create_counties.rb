class CreateCounties < ActiveRecord::Migration[5.0]
  def change
    create_table :counties do |t|
			t.references :city, foreign_key: true, index: true
			t.string :name
      t.timestamps
    end
  end
end
