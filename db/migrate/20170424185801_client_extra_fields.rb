class ClientExtraFields < ActiveRecord::Migration[5.0]
  def change
  	add_column :clients, :wechat, :string
  	add_column :clients, :whatsapp, :string
  end
end
