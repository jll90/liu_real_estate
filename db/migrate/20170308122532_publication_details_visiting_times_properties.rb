class PublicationDetailsVisitingTimesProperties < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :visiting_hrs, :string
  	add_column :properties, :publication_permissions, :string
  	add_column :properties, :published_on, :string
  end
end
