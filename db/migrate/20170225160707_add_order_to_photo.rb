class AddOrderToPhoto < ActiveRecord::Migration[5.0]
  def change
  	add_column :photos, :order, :integer, index: true, default: 0
  end
end
