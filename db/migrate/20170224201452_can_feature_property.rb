class CanFeatureProperty < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :featured, :boolean, index: true, default: false
  	remove_column :photos, :showcased
  end
end
