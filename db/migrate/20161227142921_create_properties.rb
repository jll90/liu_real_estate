class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
      t.integer :property_type, index: true
      t.string :contact_name
      t.string :contact_phone_number
      t.string :contact_email
      t.string :contact_rut
      t.boolean :is_owner, default: true
      t.string :owner_name
      t.string :owner_phone_number
      t.string :owner_email
      t.string :owner_rut
      t.string :broker_name
      t.string :broker_phone_number
      t.string :broker_email
      t.string :broker_company
      t.text :inhouse_description
      t.text :public_description
      t.integer :operation_type, index: true
      t.integer :price, index: true
      t.integer :currency, index: true
      t.integer :stage, index: true
      t.integer :usable_surface, index: true
      t.integer :rentable_surface, index: true
      t.integer :built_surface, index: true
      t.integer :terrain_surface, index: true
      t.integer :bedrooms, index: true
      t.integer :bathrooms, index: true
      t.boolean :single_space, default: false
      t.string :street_name
      t.string :street_number
      t.string :building_number
      t.string :full_address
      t.integer :storage_rooms
      t.integer :parking_spots
      t.boolean :service_room, default: false
      t.string :comune
      t.string :city
      t.string :region
      t.string :rol_number
      t.string :permalink





    end
  end
end