class AddCountyCityStateToProperties < ActiveRecord::Migration[5.0]
  def change
    remove_column :properties, :state_id
    add_reference :properties, :county, index: true, foreign_key: true
    add_reference :properties, :city, index: true, foreign_key: true
    add_reference :properties, :state, index: true, foreign_key: true
  end
end
