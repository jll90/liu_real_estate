class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.string :fullname
      t.string :email
      t.string :phone_number
      t.string :message
      t.timestamps
    end
  end
end
