class AddStateIdToProperty < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :state_id, :integer, index: true
  end
end
