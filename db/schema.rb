# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170721203601) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agencies", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "profile_img"
    t.string   "tax_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "appointments", force: :cascade do |t|
    t.string   "fullname"
    t.string   "email"
    t.string   "phone_number"
    t.string   "message"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "articles", force: :cascade do |t|
    t.text     "content"
    t.string   "title"
    t.string   "permalink"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "image_url"
    t.string   "meta_description"
  end

  create_table "cities", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_cities_on_state_id", using: :btree
  end

  create_table "clients", force: :cascade do |t|
    t.string   "fullname"
    t.string   "email"
    t.string   "rut"
    t.string   "property_url"
    t.string   "phone_number"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "description"
    t.string   "wechat"
    t.string   "whatsapp"
    t.integer  "status"
  end

  create_table "counties", force: :cascade do |t|
    t.integer  "city_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_counties_on_city_id", using: :btree
  end

  create_table "indices", force: :cascade do |t|
    t.string   "name"
    t.float    "value"
    t.datetime "reading_time"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "ownerships", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["client_id", "property_id"], name: "index_ownerships_on_client_id_and_property_id", unique: true, using: :btree
    t.index ["client_id"], name: "index_ownerships_on_client_id", using: :btree
    t.index ["property_id"], name: "index_ownerships_on_property_id", using: :btree
  end

  create_table "photos", force: :cascade do |t|
    t.integer  "property_id"
    t.string   "url"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "order",       default: 0
    t.integer  "collage"
    t.index ["property_id", "collage"], name: "index_photos_on_property_id_and_collage", unique: true, using: :btree
    t.index ["property_id"], name: "index_photos_on_property_id", using: :btree
  end

  create_table "platforms", force: :cascade do |t|
    t.string   "name"
    t.string   "website"
    t.integer  "days_to_update"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "properties", force: :cascade do |t|
    t.integer  "property_type"
    t.string   "contact_name"
    t.string   "contact_phone_number"
    t.string   "contact_email"
    t.string   "contact_rut"
    t.boolean  "is_owner",                default: true
    t.string   "owner_name"
    t.string   "owner_phone_number"
    t.string   "owner_email"
    t.string   "owner_rut"
    t.string   "broker_name"
    t.string   "broker_phone_number"
    t.string   "broker_email"
    t.string   "broker_company"
    t.text     "inhouse_description"
    t.text     "public_description"
    t.integer  "operation_type"
    t.integer  "price"
    t.integer  "stage"
    t.integer  "bedrooms"
    t.integer  "bathrooms"
    t.boolean  "single_space",            default: false
    t.string   "street_name"
    t.string   "street_number"
    t.string   "building_number"
    t.string   "full_address"
    t.integer  "storage_rooms"
    t.integer  "parking_spots"
    t.boolean  "service_room",            default: false
    t.string   "comune"
    t.string   "city"
    t.string   "region"
    t.string   "rol_number"
    t.string   "permalink"
    t.string   "title"
    t.string   "approximate_location"
    t.string   "fake_full_address"
    t.string   "neighborhood"
    t.boolean  "no_comission",            default: false
    t.string   "sales_lead_by"
    t.integer  "number_of_units"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "number_of_guests"
    t.boolean  "featured",                default: false
    t.string   "visiting_hrs"
    t.string   "publication_permissions"
    t.string   "published_on"
    t.string   "photo_layout",            default: "{\"id\": -1, \"photos\": []}"
    t.integer  "uf"
    t.boolean  "tradeable",               default: false
    t.boolean  "right_to_key",            default: false
    t.integer  "small_surface"
    t.integer  "big_surface"
    t.integer  "county_id"
    t.integer  "city_id"
    t.integer  "state_id"
    t.string   "pricing_unit"
    t.index ["bathrooms"], name: "index_properties_on_bathrooms", using: :btree
    t.index ["bedrooms"], name: "index_properties_on_bedrooms", using: :btree
    t.index ["city_id"], name: "index_properties_on_city_id", using: :btree
    t.index ["county_id"], name: "index_properties_on_county_id", using: :btree
    t.index ["operation_type", "stage"], name: "index_properties_on_operation_type_and_stage", using: :btree
    t.index ["operation_type"], name: "index_properties_on_operation_type", using: :btree
    t.index ["permalink"], name: "index_properties_on_permalink", using: :btree
    t.index ["price"], name: "index_properties_on_price", using: :btree
    t.index ["property_type", "stage"], name: "index_properties_on_property_type_and_stage", using: :btree
    t.index ["property_type"], name: "index_properties_on_property_type", using: :btree
    t.index ["stage"], name: "index_properties_on_stage", using: :btree
    t.index ["state_id"], name: "index_properties_on_state_id", using: :btree
  end

  create_table "publications", force: :cascade do |t|
    t.integer  "property_id"
    t.integer  "platform_id"
    t.integer  "days_to_update"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["platform_id"], name: "index_publications_on_platform_id", using: :btree
    t.index ["property_id"], name: "index_publications_on_property_id", using: :btree
  end

  create_table "quotes", force: :cascade do |t|
    t.string   "fullname"
    t.string   "email"
    t.string   "phone_number"
    t.string   "message"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "selling_points", force: :cascade do |t|
    t.integer  "property_id"
    t.integer  "category"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category"], name: "index_selling_points_on_category", using: :btree
    t.index ["property_id"], name: "index_selling_points_on_property_id", using: :btree
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.integer  "permission_level",  default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "phone_number"
    t.string   "national_id"
    t.index ["email"], name: "index_users_on_email", using: :btree
  end

  create_table "visits", force: :cascade do |t|
    t.integer  "property_id"
    t.integer  "client_id"
    t.datetime "time"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["client_id"], name: "index_visits_on_client_id", using: :btree
    t.index ["property_id"], name: "index_visits_on_property_id", using: :btree
  end

  add_foreign_key "cities", "states"
  add_foreign_key "counties", "cities"
  add_foreign_key "ownerships", "clients"
  add_foreign_key "ownerships", "properties"
  add_foreign_key "photos", "properties"
  add_foreign_key "properties", "cities"
  add_foreign_key "properties", "counties"
  add_foreign_key "properties", "states"
  add_foreign_key "publications", "platforms"
  add_foreign_key "publications", "properties"
  add_foreign_key "selling_points", "properties"
  add_foreign_key "visits", "clients"
  add_foreign_key "visits", "properties"
end
