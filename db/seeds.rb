# SEED_STATES = [
# 	"Tarapacá",
# 	"Antofagasta",
# 	"Atacama",
# 	"Coquimbo",
# 	"Valparaíso",
# 	"Libertador General Bernardo O'Higgins",
# 	"Maule",
# 	"Biobío",
# 	"Araucanía",
# 	"Los Lagos",
# 	"Aysén del General Carlos Ibáñez del Campo",
# 	"Magallanes y de la Antártica Chilena",
# 	"Metropolitana de Santiago",
# 	"Los Ríos",
# 	"Arica y Parinacota"
# ]

# STATES.each do |s|
# 	State.create!(name: s)
# end

CITIES = [
	["Iquique", "Tamarugal"],
	["Antofagasta", "El Loa", "Tocopilla"],
	["Chañaral", "Copiapó", "Huasco"],
	["Choapa", "Elqui", "Limarí"],
	["Isla de Pascua", "Los Andes", "Marga Marga", "Petorca", "Quillota", "San Antonio", "San Felipe de Aconcagua", "Valparaíso"],
	["Cachapoal", "Cardenal Caro", "Colchagua"],
	["Cauquenes", "Curicó", "Linares", "Talca"],
	["Arauco", "Biobío", "Concepción", "Ñuble"],
	["Cautín", "Malleco"],
	["Chiloé", "Llanquihue", "Osorno", "Palena"],
	["Aysén", "Capitán Prat", "Coyhaique", "General Carrera"],
	["Antártica Chilena", "Magallanes", "Tierra del Fuego", "Última Esperanza"],
	["Chacabuco", "Cordillera", "Maipo", "Melipilla", "Santiago", "Talagante"],
	["Ranco", "Valdivia"],
	["Arica", "Parinacota"]
]

CITIES.each_with_index do |c, i|
	c.each do |sc|
		City.create!(name: sc, state_id: i+1)
	end
end