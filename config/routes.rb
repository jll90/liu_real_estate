Rails.application.routes.draw do  
  
  resources :agencies
  resources :ownerships
  resources :publications
  resources :platforms
  resources :visits
  resources :clients
  resources :appointments

  root 'main#home'
	post 'authenticate', to: 'authentication#authenticate'
  post 'process_csv' => "mailer#send_multiple"

  post "/mailer/get_to_know_us" => "mailer#get_to_know_us"
  post "/mailer/send_welcome_email" => "mailer#send_welcome_email"

  get 'sitemap.xml', to: 'sitemap#index', defaults: {format: 'xml'}

  get 'googlebb9b50f00baf5445.html', to: 'main#google_verification'

  get 'robots.:format',to: 'robots#index'

	resources :users

  resources :articles, path: 'blog'
  resources :quotes, only: [:index, :new, :create]
  resources :clients
  resources :publications

	get '/properties/code/:code', to: 'properties#code'
  get '/properties/suggestion', to: 'properties#suggestion'
	get '/properties/:id/private_profile', to: 'properties#private_show'
	get '/properties/:id/edit', to: 'properties#edit'
  get '/properties/filtered_index', to: 'properties#filtered_index'
  get '/properties/featured_index', to: 'properties#featured_index'
  get '/properties/just_listed_index', to: 'properties#just_listed_index'

  resources :properties do
	  resources :selling_points
  	resources :photos, except: [:destroy, :new]
  end

	patch '/properties/:propertyId/photos', to: 'photos#update_all'

  #must be placed before the next route
  get '/jobs/twitter', to: 'jobs#twitter'

  #refactor using shallow true
  resources :photos, only: [:destroy, :update]

  resources :indices, only: [:create, :index]

  get '/places', to: 'places#index'

end
