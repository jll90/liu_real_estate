PROPERTY_TYPES = 
	[
		"house",
		"apartment",
		"office",
		"commercial",
		"industrial",
		"storage",
		"site",
		"land",
		"parking"
	]

def property_name_translator (id)
	translated_name = case id
		when 0 then "casa"
		when 1 then "departamento"
		when 2 then "oficina"
		when 3 then "local comercial"
		when 4 then "industrial"
		when 5 then "bodega"
		when 6 then "sitio"
		when 7 then "terreno"
		when 8 then "estacionamiento"
	end
end

CLIENT_STATUS = [
	"no status",
	"no contact",
	"rejected service",
	"must sign documents",
	"must upload property",
	"waiting for an offer",
	"promise of sale or rent",
	"final contract (sale or rent)",
	"sold with us",
	"looking for a property",
	"needs more information",
	"waiting to schedule visit"
]

def as_hashtag (string)
	hashtag = "\##{string}"
end

OPERATION_TYPES = 
	[
		"sale",	
		"rent",
		"seasonal rent"
	]	

STAGES = [
	"No contact",
	"Rejected - Information Pending",
	"Accepted - Pending Visit",
	"Pending Information Upload",
	"Marketing",
	"Negotiation in progress",
	"Operation completed",
	"Data only"
]

SELLING_POINT_CATEGORIES = [
	{
		id: 0,
		category: "Home"
	},
	{
		id: 1, 
		category: "Facilities"
	},
	{
		id: 2,
		category: "Location"
	},
	{
		id: 3,
		category: "Terrain"
	}
]

STATES = [
	"I de Arica y Parinacota",
	"II de Tarapacá",
	"III de Antofagasta",
	"IV de Atacama",
	"V de Coquimbo",
	"VI de Valparaíso",
	"VII de O'Higgins",
	"VIII del Maule",
	"IX del Biobío",
	"X de la Araucanía",
	"XI de los Ríos",
	"XII de los Lagos",
	"XIII de Aisén",
	"XIV de Magallanes",
	"XV Metropolitana"
]

VALID_STAGES_FOR_SHOWCASING = [4, 5, 6]

MAILER_MESSAGES = {
	rent_property: "Nos gustaría ayudarle a promocionar su propiedad
	y encontrar al arrendatario ideal lo más rápido posible.",
	default: "Nos encantaría ayudarle a negociar las propiedades que usted nos confíe, y 
	que cuente con nosotros para encontrar el tipo de inmueble que usted está buscando.",
	sell_property: "En esta oportunidad, nos gustaría ayudarle a encontrar al comprador ideal y 
	facilitar la venta de su propiedad."
}

MAILER_RENT_MESSAGE = :rent_property
MAILER_DEFAULT_MESSAGE = :default
MAILER_SALE_MESSAGE = :sell_property

MAILER_BLACKLIST = [
	'terapeutaintegral@gmail.com',
	'gloriapazv@gmail.com',
	'xj.ferkoch@gmail.com',
	'aercriverosc@hotmail.com',
	'firarrazsch@yahoo.com', #client
	'xime.molina@gmail.com', #agent
	'caratoro@gmail.com', #agent
	"doyarzunr@gmail.com", #client
	"maryanneargo@gmail.com", #agent
	"augustocornejo@gmail.com", #agent
	"lorena@ventasmaremosso.cl", #agent
	"nqpuga@gmail.com", #agent
	"lcardenas7779@gmail.com", #agent
	"mp.ferrer.orellana@gmail.com", #partner agent,
	"mireya_personal@hotmail.com", #partner agent
	"mardeaux@gmail.com", #partner agent
	"moralesfalcon@gmail.com", #client
	"mygarriendos@gmail.com", #client
	"mcuesta@miuandes.cl", #client
	"rvfuchslocher@gmail.com", #client
	"rositacarom992@gmail.com", #client
	"jncortes@uc.cl", #client
	"mariaraqueldiaz@terra.cl", #client
	"lily.avila@gmail.com", #client
	"valdes.sonia@gmail.com", #agent
	"martapuigr@gmail.com", #client
	"mravello1@gmail.com", #agent
	"nicolas.reyes@jgcia.cl", #mean client
	"ioyanedel@sinacofi.cl", #agent
	"sarita.manri@gmail.com", #could become partner agent
	"pedrocorreak@gmail.com", #agent
	"eurzuajara@gmail.com", #agent
	"rodrigocastellongiroz@gmail.com", #semi parter agent
	"ssalas_jofre@hotmail.com", #agent (friendly)
	"mgmellado@gmail.com", #agent
	"pams@vtr.net", #agent friendly
	"maria.jose.fyf@gmail.com", #agent friendly,
	"gabrielitaale1979@gmail.com",
	"jaretahe@yahoo.com",
	"gabyx@vtr.net",
	"documentos@valesmo.com",
	"vjskaric@gmail.com",
	"natalia.olivares@duplexgi.cl",
	"admin@condominioterranova.cl",
	"m.francisca.altamirano@gmail.com",
	"luispardo.c@gmail.com",
	"f.ortiz.ureta@gmail.com",
	"contacto@urbeactiva.cl",
	'cruzugarte@gmail.com',
	'favelpo@gmail.com',
	'rgoparedes@gmail.com',
	'azucenavenegas@gmail.com',
	'delagos.jimenez@gmail.com',
	'ra.ortega.g@gmail.com',
	'ccastillo@ictmv.cl',
	"m.francisca.altamirano@gmail.com",
	"sgomeze@udd.cl",
	'mrespino@uc.cl',
	'cgmora@vivasuespacio.cl',
	'claudia.munoz@comlarrain.cl',
	'cifuentescecilia@gmail.com',
	'alejandra.metrohouse@gmail.com',
	'm.carbonetto.m@gmail.com',
	'rraineri@ing.puc.cl',
	'alexis2225@gmail.com',
	'datiar.negocio@gmail.com',
	'mcvilarrubias@gmail.com',
	'turnhabilis@gmail.com',
	'patricio.plopez@gmail.com',
	'delsolargonzalo@gmail.com',
	'barbara.abeliuk@gmail.com',
	'freddy.gallardo@hotmail.es',
	'a.diaz1994@hotmail.com',
	'carolina.gallardo.castillo@gmail.com',
	'kavonber@gmail.com',
	'bpsagro@gmail.com',
	'claudiafcn@gmail.com',
	'myriam_aranguiz@hotmail.com',
	'secretariacontable2063@hotmail.com',
	'giovannaromejas@gmail.com',
	'alejandra.amoreno@gmail.com',
	'contacto@focalizate.cl',
	'maritza.aranki@gmail.com',
	'turnhabilis@gmail.com',
	'cgmora@vivasuespacio.cl',
	'favelpo@gmail.com',
	'd.malhuevillegas@gmail.com',
	'marquinones@gmail.com',
	'cavila@constructoraick.cl',
	'felhur@yahoo.es',
	'b.valenzuela.u@gmail.com',
	'delsolargonzalo@gmail.com',
	'barbara.abeliuk@gmail.com',
	'pa.blokuz@gmail.com',
	'salomon3lopez@yahoo.es',
	'reddy.gallardo@hotmail.es',
	'galbornoz.rojas@gmail.com',
	'alejandro.uribet@gmail.com',
	'joselinmorales1106@gmail.com',
	'alroga_83@yahoo.es',
	'perazor2015@gmail.com',
	'mauricio.castillo.e@gmail.com',
	'tvproac@gmail.com',
	'paola.carolina.bravo@gmail.com',
	'bpsagro@gmail.com',
	'claudiafcn@gmail.com',
	'mfuenzalida@bellfuenzalida.cl',
	'urrutia.hernan@gmail.com',
	'deptoprovidencia2016@gmail.com',
	'lfuentes@grupomaqui.cl',
	'flgarrid@outlook.com',
	'snicolasurf@gmail.com',
	'gouprovidencia@gmail.com',
	'pattybastias@gmail.com',
	'abian.vallejos@reistock.com',
	'cgaray@fattos.cl',
	'kavonber@gmail.com',
	'g.pacheco.a@gmail.com',
	'zuazagoitia@hotmail.com',
	'paola.bravo_69@gmail.com',
	'aservatfx@gmail.com',
	'agente@stoffel.cl',
	'c.ortizeaton@gmail.com',
	'urrutia.hernan@gmail.com',
	"catavaccaro@gmail.com",
	"cesanchezag@gmail.com",
	"javierpargay@hotmail.com"
]
