#This file contains configuration for 
#include and exclude params

PROPERTY_EXCEPT_PARAMS = [
		:building_number,
		:contact_name,
		:contact_phone_number,
		:contact_email,
		:contact_rut,
		:is_owner,
		:owner_name,
		:owner_phone_number,
		:owner_email,
		:owner_rut,
		:broker_name,
		:broker_phone_number,
		:broker_email,
		:broker_company,
		:inhouse_description,
		:street_number,
		:full_address,
		:rol_number,
		:sales_lead_by,
		:visiting_hrs,
		:publication_permissions,
		:published_on,
		:stage,
		:street_name
	]

USER_EXCEPT_PARAMS = [
	:created_at, 
	:password_digest, 
	:permission_level, 
	:activation_digest, 
	:national_id, 
	:remember_digest
]